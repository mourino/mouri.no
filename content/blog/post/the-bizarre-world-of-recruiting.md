---
title: "The bizarre world of recruiting"
date: 2019-09-19T10:55:00-05:00
draft: false
---
Linkedin is a weird place. You upload your work history, connect with people
(some of which you worked with), and get contacted by companies looking for
people with your profile. At least that's what it should be. The reality,
however, is a little different.

Let's start with the work history. Besides adding a detailed list of my work
experience (both the companies I worked for and the side projects I had)
recruiters always ask me for a good ol' CV in PDF format. Why did I bother look
up the dates I joined and left those first jobs I had? All they seemed to care
before contacting me was:

* Years of experience.
* Tech I worked with.
* Some big name I worked for.

Then you _connect_ with people. I'm still to see the value of this. At best you
can stalk your ex colleagues and see where they are working at without having
to talk with them. But it's actually pointless, it would not change a thing if
you didn't connect with anybody.

Finally, you get contacted by recruiters. This is the weirdest part. A random
person (for some reason they tend to be very good looking) throws a bunch of
buzzwords at you with some eye-catching title, in an e-mail that looks like a
robot is flirting with you. If some of that nonsense sticks and you write back
asking for clarification, they will set up a call. It has to be a call.

If you are lucky in this phone call they will ask you no more than 1 awkward
question. Usually the awkwardness extends over a good portion of the call,
and will leave you wondering if that weird exchange actually happened. Don't
worry, most of the times this will be the last thing you will ever hear from
that person. For some reason most recruiters show up, suck up your data, and
then disappear back into the email-copy-pasting world they live in.

The few that actually deliver on their promise of _recruiting_ will send you
the only 1 or 2 positions they are recruiting for, which means that the talk
was pointless and they could have just sent you those openings right away. At
this point you may think that you made it, now you are just going to start
having interviews. Well, most likely the recruiter _forgot_ to mention that the
position requires native level of German, or that it's a leadership role when
you have no leadership experience.

Once you sort all that out, and you go through your first interview, be ready
to start doing your recruiter's work and start contacting both the company and
the recruiter to ask them if they decided to hire someone else, or why you
haven't heard from them in 2 weeks. Most likely your recruiter is on holidays
and nobody told you, nor did they handover your profile to anybody else. It's
like they keep your data in a paper notebook in someone's locker who takes the
key with them every night.

Keep in mind that no matter how bad the recruiting process is, that's rarely a
reflection of the company itself. _Don't judge a company by their recruiting
process_ they say. However, the opposite tends to be true in my experience:
good and smooth recruiting processes tend to be a sign of a good place to work.
But don't quote me on that.
